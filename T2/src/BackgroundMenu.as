package  
{
	import org.flixel.*;	
	 
	public class BackgroundMenu extends FlxSprite
	{
		[Embed(source = "../assets/Mapa/fundo.png")] private var FUNDO:Class;
		[Embed(source="../assets/Mapa/fundo_map.png")] private var FUNDO_JOGO:Class;
		public function BackgroundMenu(jogo:Boolean = false) 
		{
			if(jogo) loadGraphic(FUNDO_JOGO, false, false, 1600, 640);
			else loadGraphic(FUNDO, false, false, 320, 240);
		}
		
	}

}