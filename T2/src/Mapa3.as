package  
{
	import org.flixel.*;
	import org.flixel.system.FlxTile;

	public class Mapa3 extends Mapa
	{
		[Embed(source = "../assets/Mapa/CSV3/mapCSV_MAPA_Mapa.csv", mimeType = "application/octet-stream")] protected var T1:Class;
		[Embed(source = "../assets/Mapa/CSV3/mapCSV_MAPA_Danos.csv", mimeType = "application/octet-stream")] protected var T4:Class;
		[Embed(source = "../assets/Mapa/CSV3/mapCSV_MAPA_Firulas.csv", mimeType = "application/octet-stream")] protected var T3:Class;
		[Embed(source = "../assets/Mapa/CSV3/mapCSV_MAPA_GramaNegocios.csv", mimeType="application/octet-stream")] protected var T2:Class;
		[Embed(source = "../assets/Mapa/CSV3/mapCSV_MAPA_Objetos.csv", mimeType = "application/octet-stream")] protected var T5:Class;
		
		override public function create():void 
		{
			certa = 2;
			carregaTiles(T1, T2, T3, T4, T5);
			restart(33, 200);
		}
		
		override public function update():void{	
			atualiza();
			super.update();
		}
		
		override protected function fimDeJogo(p:FlxObject, d:FlxObject):void {
		//if(FlxG.keys.justPressed("UP")) {
			FlxG.play(WIN);
			personagem.vivo = false;
			FlxG.switchState(new Menu(true));
		//}
		}
	}
}