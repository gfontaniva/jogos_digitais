package  
{
	import org.flixel.*;
	import org.flixel.system.FlxTile;

	public class Mapa extends FlxState
	{
		protected var level		:int;
		protected var personagem  :Personagem;
		protected var tiles1		:FlxTilemap;
		protected var tiles2		:FlxTilemap;
		protected var tiles3		:FlxTilemap;
		protected var tiles4		:FlxTilemap;
		protected var objetos		:FlxTilemap;
		protected var bkgr		:BackgroundMenu;
		protected var timer		:FlxTimer;
		protected var timerDano	:FlxTimer;
		protected var reload		:Boolean;
		protected var tomouDano	:Boolean;
		protected var chaveCorreta:Boolean;
		protected var textoMorte	:FlxText;
		protected var live 		:Live;
		protected var diamantes 	:FlxGroup;
		protected var chaves 		:FlxGroup;
		protected var travas 		:FlxGroup;
		protected var portas		:FlxGroup;
		protected var obj			:FlxTilemap;
		protected var certa			:int;
		
		[Embed(source = "../assets/Musicas/Invasion.mp3")] protected var MUSICA_FUNDO_MP3:Class;
		[Embed(source = "../assets/Musicas/eviljest.mp3")] protected var EFEITO_MORREU_MP3:Class;
		[Embed(source = "../assets/Mapa/pubdlcnt.png")] protected var TILES:Class;
		[Embed(source = "../assets/Musicas/coin7.mp3")] protected var PEGOU: Class;
		[Embed(source = "../assets/Musicas/getKey.mp3")] protected var CHAVE_MP3: Class;
		[Embed(source = "../assets/Musicas/unlock.mp3")] protected var UNLOCK:Class;
		[Embed(source = "../assets/Musicas/win.mp3")] protected var WIN:Class;
		
		protected function carregaTiles(TILESBKG1, TILESBKG2, TILESBKG3, TILESBKG4, OBJETOS): void {
			definicoes();
			
			reload = false;
			tiles1 = new FlxTilemap();
			tiles1.loadMap(new TILESBKG1, TILES, 16, 16, FlxTilemap.OFF, 0);
			tiles2 = new FlxTilemap();
			tiles2.loadMap(new TILESBKG2, TILES, 16, 16, FlxTilemap.OFF, 0);
			tiles3 = new FlxTilemap();
			tiles3.loadMap(new TILESBKG3, TILES, 16, 16, FlxTilemap.OFF, 0);
			tiles4 = new FlxTilemap();
			tiles4.loadMap(new TILESBKG4, TILES, 16, 16, FlxTilemap.OFF, 0);
			obj = new FlxTilemap();
			obj.loadMap(new OBJETOS, TILES, 16, 16);
			
			tiles2.setTileProperties(975, FlxObject.NONE, caiuAgua, null, 1);	
			tiles4.setTileProperties(977, FlxObject.NONE, caiuAgua, null, 1);
			tiles4.setTileProperties(1039, FlxObject.NONE, caiuAgua, null, 1);
			tiles4.setTileProperties(975, FlxObject.NONE, caiuAgua, null, 1);
			
			tiles4.setTileProperties(1030, FlxObject.NONE, tomaDano, null, 1);
			tiles4.setTileProperties(344, FlxObject.NONE, tomaDano, null, 1);
			tiles4.setTileProperties(600, FlxObject.NONE, tomaDano, null, 1);
			tiles4.setTileProperties(1094, FlxObject.NONE, tomaDano, null, 1);
			tiles4.setTileProperties(536, FlxObject.NONE, tomaDano, null, 1);
			
			add(tiles1);
			add(tiles2);
			add(tiles3);
			add(tiles4);
			add(diamantes);
			add(chaves);
			add(travas);
			add(portas);
			add(textoMorte);
			add(live);
			criaObjetos();
			personagem = new Personagem();
			this.restart();
			add(personagem);
			
			FlxG.camera.follow(personagem);		
			FlxG.camera.setBounds(0, 0, tiles1.width, tiles1.height, true);	
		}
		
		protected function definicoes(): void {
			FlxG.playMusic(MUSICA_FUNDO_MP3, 0.6);
			FlxG.bgColor = 0xff87CEFA;
			FlxG.mouse.hide();
			//bkgr = new BackgroundMenu(true);
			//add(bkgr);
			live = new Live;
			FlxG.score = 0;
			live.scrollFactor.x = 0;
			live.scrollFactor.y = 0;
			
			chaveCorreta = false;
			diamantes = new FlxGroup;
			chaves = new FlxGroup;
			travas = new FlxGroup;
			portas = new FlxGroup;
			
			timer = new FlxTimer;
			timerDano = new FlxTimer;
			tomouDano = false;
			textoMorte = new FlxText(0, 0, 300, "Você Morreu!");
			textoMorte.setFormat(null, 15, 0xffff0000);
			textoMorte.visible = false;
		}

		protected function pegaDiamante(p:FlxObject, d:FlxObject):void {
			d.kill();
			FlxG.play(PEGOU, 0.4);
		}
		
		protected function pegaChave(p:FlxObject, d:FlxObject):void {
			if ((d as Chave).tipo = certa) chaveCorreta = true;
			d.kill();
			FlxG.play(CHAVE_MP3);
		}
		
		protected function destrava(p:FlxObject, d:FlxObject):void {
			d.immovable = true;
			if(chaveCorreta) {
				d.kill();
				FlxG.play(UNLOCK);
			}
		}
		
		protected function fimDeJogo(p:FlxObject, d:FlxObject):void {

		}
		
		protected function tomaDano(t:FlxTile, o:FlxObject) :void {
			//if (!player1.flickering) {
				if (!tomouDano) {
					timerDano.start(0.5);
					personagem.flicker(0.1);
					live.valor -= 1;
					if (live.valor <= 0) {
						morreu(0);
					}
					tomouDano = true;
				}
			//}
		}
		
		protected function ignora(t:FlxTile, o:FlxObject) :void {
			//if (!player1.flickering) {
				FlxG.log("Ignora!!");
			//}
		}
		
		protected function restart(x = 33, y = 90):void {
			personagem.renasce();
			personagem.x = x;
			personagem.y = tiles1.height - y;
			personagem.flicker(0.2);
			live.valor = 4;
		}
		
		protected function morreu(i : int):void {
			if (!reload) {
				personagem.morreu();
				FlxG.play(EFEITO_MORREU_MP3, 0.6);
				reload = true;
				textoMorte.visible = true;
				//textoMorte.alignment = "center";
				textoMorte.x = personagem.x - 10;
				textoMorte.y = personagem.y - 100;
				//textoMorte.getScreenXY(null, cameras);
				FlxG.log("Morreu, e continua dando!!");
				timer.start(1.5);
				switch (i){
					case 1: //morreu na agua
						live.valor = -1;
						break;
					case 2: //morreu outroas coisas
						break;
				}
			}
			
		}
		
		protected function criaObjetos():void {
			for (var ty:int = 0; ty <= obj.heightInTiles; ty++)
			{
				for (var tx:int = 0; tx <= obj.widthInTiles; tx++)
				{				
					switch (obj.getTile(tx, ty)) {
						case (449): //Diamantes
							diamantes.add(new Diamante(2, tx,ty));
							break;
						case (448): //Diamantes
							diamantes.add(new Diamante(1, tx,ty));
							break;
						case (450): //Diamantes
							diamantes.add(new Diamante(4, tx,ty));
							break;
						case (451): //Diamantes
							diamantes.add(new Diamante(3, tx,ty));
							break;
						case (256): //chave amarela
							chaves.add(new Chave(1, tx, ty));
							break;
						case (257): //chave verde
							chaves.add(new Chave(2, tx, ty));
							break;
						case (258): //chave vermelha
							chaves.add(new Chave(3, tx, ty));
							break;
						case (259): //chave azul
							chaves.add(new Chave(4, tx, ty));
							break;
						case (320):
							travas.add(new Fechadura(0 , tx, ty));
							break;
						case (321):
							travas.add(new Fechadura(1 , tx, ty));
							break;
						case (322):
							travas.add(new Fechadura(2 , tx, ty));
							break;
						case (323):
							travas.add(new Fechadura(3 , tx, ty));
							break;
						case (897):
							portas.add(new Porta(1, tx, ty));
							break;
						case (208):
							portas.add(new Porta(2, tx, ty));
							break;
					}
				}
			}
		}
		
		protected function atualiza():void {
			FlxG.collide(personagem, tiles1);
			FlxG.collide(personagem, tiles2);
			FlxG.collide(personagem, tiles4);
			FlxG.collide(personagem, diamantes, pegaDiamante);
			FlxG.collide(personagem, chaves, pegaChave);
			FlxG.collide(personagem, travas, destrava);
			FlxG.collide(personagem, portas, fimDeJogo);
			
			//morte afogamento
			if (reload && timer.finished) {
				restart();
				reload = false;
				textoMorte.visible = false;
			}
			//tomou dano
			if (tomouDano && timerDano.finished) {
				tomouDano= false;
			}
		}
		
		protected function caiuAgua(tile :FlxTile, object :FlxObject):void
		{
			morreu(1);
		}
	}
}