package  
{
	import org.flixel.*;
	 
	public class Diamante extends FlxSprite 
	{
		public var pontuacao: int;
		private var tipo: int;
		
		[Embed(source = "../assets/diamantes.png")]  private var DIAMANTES:Class;
		[Embed(source = "../assets/Musicas/coin7.mp3")] private var PEGOU: Class;
		
		public function Diamante(t : int, X: int, Y: int) 
		{
			pontuacao = t * 2;
			this.tipo = t;
			super(X * 16, Y * 16);
			
			FlxG.score += pontuacao;
			
			loadGraphic(DIAMANTES, true, false, 16, 16);
			
			addAnimation("azul", [2], 0);
			addAnimation("vermelho", [3], 0);
			addAnimation("verde", [1], 0);
			addAnimation("amarelo", [0], 0);
			
			switch(t){
				case 1:
					play("amarelo");
					break;
				case 2:
					play("verde");
					break;
				case 3:
					play("azul");
					break;
				case 4:
					play("vermelho");
					break;
			}
		}
		
		public function pegouDiamante(): void {
			FlxG.play(PEGOU, 0.5);
			this.kill();
		}
		
	}

}