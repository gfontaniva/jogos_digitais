package  
{
	import org.flixel.*;

	public class Chave extends FlxSprite
	{
		[Embed(source = "../assets/chaves.png")] private var CHAVES:Class;
		[Embed(source = "../assets/Musicas/getKey.mp3")] private var CHAVE_MP3:Class;
		
		public var tipo: int;
		
		public function Chave(t : int, X: int, Y: int) 
		{
			this.tipo = t;
			super(X * 16, Y * 16);
			
			loadGraphic(CHAVES, true, false, 16, 16);
			addAnimation("amarela", [1], 0);
			addAnimation("verde", [2], 0);
			addAnimation("vermelho", [3], 0);
			addAnimation("azul", [4], 0);

			switch(t){
				case 1:
					play("amarela");
					break;
				case 2:
					play("verde");
					break;
				case 3:
					play("vermelho");
					break;
				case 4:
					play("azul");
					break;
			}
		}
		
		public function pegouChave(): int {
			FlxG.play(CHAVE_MP3);
			this.kill();
			return tipo;
		}
		
	}

}