package  
{
	import adobe.utils.CustomActions;
	import org.flixel.*;

	public class Menu extends FlxState
	{
		[Embed(source="../assets/Musicas/Trouble Makers.mp3")] private var MUSICA_FUNDO_MP3:Class;
			
		private var start		:FlxText;
		private var textoWin	:FlxText;
		private var fundo		:BackgroundMenu;
		private var btnPlay		:FlxButton;
		private var btnCreditos	:FlxButton;
		
		public function Menu(venceu :Boolean = false) 
		{
			if (venceu) {
				textoWin = new FlxText(50, 0, 180, "Você venceu! Pontuação: " + FlxG.score);
				textoWin.color 				= 0xff0000;
				textoWin.alignment 			= "center";
				textoWin.shadow 			= 0xff000000;
				textoWin.size 				= 15;
				textoWin.scrollFactor.x 	= 0;
				textoWin.scrollFactor.y 	= 0;
			}
		}
		
		override public function create():void 
		{
			FlxG.playMusic(MUSICA_FUNDO_MP3);
			FlxG.mouse.show();
			
			fundo = new BackgroundMenu();
			add(fundo);
			
			btnPlay = new FlxButton(230, 165, "Jogar", goPlayState);
			btnCreditos = new FlxButton(230, 190, "Créditos", goCreditState);
			
			add(btnPlay);
			add(btnCreditos);
			add(textoWin);
		}
		
		private function goPlayState():void {
			FlxG.switchState(new Mapa1());
		}
		
		private function goCreditState():void {
			FlxG.switchState(new Creditos);
		}

	}
}