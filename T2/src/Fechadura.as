package  
{
	import org.flixel.*;
	
	public class Fechadura extends FlxSprite
	{
		[Embed(source = "../assets/fechadura.png")] private var FECHADURA:Class;
		[Embed(source="../assets/Musicas/unlock.mp3")] private var UNLOCK:Class;
		
		private var tipo: int;
		
		public function Fechadura(t : int, X: int, Y: int) 
		{
			this.tipo = t;
			super(X * 16, Y * 16);
			loadGraphic(FECHADURA, true, false, 16, 16);
			addAnimation("amarela", [0], 0);
			addAnimation("verde", [1], 0);
			addAnimation("vermelho", [2], 0);
			addAnimation("azul", [3], 0);

			switch(t){
				case 0:
					play("amarela");
					break;
				case 1:
					play("verde");
					break;
				case 2:
					play("vermelho");
					break;
				case 3:
					play("azul");
					break;
			}
		}
		
		public function abreFechadura(): void {
			FlxG.play(UNLOCK);
			this.kill();
		}
	}

}