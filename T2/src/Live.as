package  
{
	import org.flixel.*;

	public class Live extends FlxSprite
	{
		[Embed(source="../assets/coracao.png")] private var CORACAO:Class;
		public var valor: int;
		public function Live() 
		{
			loadGraphic(CORACAO, true, false, 16, 16);
			valor = 4;
			x = y = 5;
			addAnimation("4", [0], 5);
			addAnimation("3", [1], 5);
			addAnimation("2", [2], 5);
			addAnimation("1", [3], 5);
			addAnimation("0", [4], 5);
			addAnimation("caiu", [0,1,2,3,4,4,4,4,4,4], 4);
			
			 play("4");
			
		}
		
		
		override public function update():void 
		{
			if(valor >= 0){
				switch (valor) {
					case 4: 
						play("4");
						break;
					case 3: 
						play("3");
						break;
					case 2: 
						play("2");
						break;
					case 1: 
						play("1");
						break;
					case 0: 
						play("0");
						break;
					case -1: 
						play("caiu");
						this.flicker(0.1);
						break;
				}
			}
		}
	}

}