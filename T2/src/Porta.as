package  
{
	import org.flixel.*;
	
	public class Porta extends FlxSprite
	{
		[Embed(source = "../assets/porta.png")] private var PORTA:Class;
		
		private var tipo: int;

		public function Porta(t : int, X: int, Y: int) 
		{
			this.tipo = t;
			super(X * 16, Y * 16);
			loadGraphic(PORTA, true, false, 16, 16);
			addAnimation("cima", [0], 0);
			addAnimation("baixo", [1], 0);
			
			switch(t){
				case 1:
					play("cima");
					break;
				case 2:
					play("baixo");
					break;
			}
		}
		
	}

}