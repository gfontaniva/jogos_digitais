package  
{
	import org.flixel.*;
	
	public class Creditos extends FlxState
	{
		[Embed(source="../assets/Musicas/Trouble Makers.mp3")] private var MUSICA_FUNDO_MP3:Class;
		
		private var btnBack	:FlxButton;
		private var fundo	:BackgroundMenu;
		private var nomes	:FlxText;
		
		override public function create():void 
		{
			FlxG.playMusic(MUSICA_FUNDO_MP3);
			FlxG.mouse.show();
			
			fundo = new BackgroundMenu();
			add(fundo);
			
			btnBack = new FlxButton(230, 190, "Voltar", goMenu);

			nomes 					= new FlxText(160, 100, 110, "Guilherme Fontaniva Geomar Schreiner Lucas Martins");
			nomes.alignment 		= "center";
			nomes.shadow 			= 0x0000FF;
			nomes.size 				= 8;
			nomes.scrollFactor.x 	= 0;
			nomes.scrollFactor.y 	= 0;
			
			add(nomes);
			add(btnBack);
		}
		
		private function goMenu():void
		{
			FlxG.switchState(new Menu);
		}
		
	}

}