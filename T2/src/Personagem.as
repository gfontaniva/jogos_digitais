package  
{
	import org.flixel.*;
	
	public class Personagem extends FlxSprite
	{
		[Embed(source = "../assets/Personagem/pubdlcnt.png")] private var PERSONAGEM:Class;
		[Embed(source = "../assets/Personagem/jump_01.mp3")] private var PULO_MP3:Class;
			
		public var direita:Boolean;
		public var vivo:Boolean;
		
		public function Personagem() 
		{
			direita = true;
			vivo = true;
			loadGraphic(PERSONAGEM, true, false, 22, 22);
			maxVelocity.x = 80;   // Theses are physics settings,
			maxVelocity.y = 700;  // controling how the players behave
			acceleration.y = 500; // in the game
			drag.x = this.maxVelocity.x * 4;
			
			addAnimation("paraR", [0,1,2], 5);
			addAnimation("paraL", [27, 28, 29], 5);
			addAnimation("andaR", [3,4,5], 5);
			addAnimation("andaL", [30, 31, 32], 5);
			addAnimation("morreR", [24,25,26], 5);
			addAnimation("morreL", [51, 52, 53], 5);
		}
		
		override public function update():void 
		{
			super.update();
			
			if (!FlxG.keys.any() && vivo) {
				if (direita) play("paraR");
				else play("paraL");
			}
			
			if (FlxG.keys.pressed("LEFT") && vivo && x >= 2) {
				direita = false;
				x -= 2;
				play("andaL");
			}
			
			if (FlxG.keys.pressed("RIGHT") && vivo && (x < 1580)) {
				FlxG.log(x);
				direita = true;
				x += 2;
				play("andaR");
			}
			
			if (FlxG.keys.justPressed("UP") && isTouching(FlxObject.FLOOR) && vivo) {
				FlxG.play(PULO_MP3, 0.6);
				velocity.y -= maxVelocity.y/2.2;
			}
		}
		
		public function morreu(): void
		{
			vivo = false;
			if (direita) play("morreR");
			else play("morreL");
		}
		
		public function renasce(): void
		{
			play("paraR");
			direita = true;
			vivo = true;
		}
	}

}